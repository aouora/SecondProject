#產生一個0~100的隨機數給user猜數字
#猜對後統計猜測次數且詢問是否繼續玩
#只能回答(是/否)防呆設計
import random
while True:
    answer = random.randint(1, 100)
    time = 0
    while True:
        try:
            number = int(input("請猜一個數字："))
        except ValueError:
            print("請輸入有效的數字！")
            continue
        time += 1
        if number < answer:
            print('比答案小') 
        elif number > answer:
            print('比答案大')
        else:
            print("終於猜對了，總共猜了", time, "次") 
            while True:
                again = input("要繼續玩嗎？(是/否)：")
                if again == '是':
                    answer = random.randint(1, 10)  # 產生新的隨機數
                    time = 0  # 重置猜測次數
                    break
                elif again == '否':
                    exit(0)  # 直接退出整個程式
                else:               
                    print('請輸入有效的選擇 ("是" 或 "否")！')
        

